function [V] = get_data_ECON(fn, fnPreFix, lineDataStart, nLines, nFiles)
% fn - The directory name.
% fnPreFix - The file prefix.
% lineDataStart - The line number of the data start.
% nLines - The data lines wanted.
% nFiles - Number of data files.
% V - The data obtained from the file.


% Initial argument check.

V = 0;

if ( lineDataStart <= 0 || nLines <= 0 || nFiles <= 0 )
    fprintf('Wrong arguments. lineDataStart = %d, nLines = %d, nFiles = %d\n',...
        lineDataStart, nLines, nFiles);
    return;
end

V = zeros(nLines, nFiles);

for I = 1:1:nFiles
    fullFileName = sprintf('%s/%s%d.txt', fn, fnPreFix, I);
    
%     fprintf('%d,', I);
    
    [ch] = get_data_from_file_ECON(fullFileName, lineDataStart, nLines);
    
    V(:, I) = ch;
end % I

% fprintf('\n');

