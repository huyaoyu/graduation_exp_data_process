close all;

offset = 45;
theta = ( [30, 150, -90] + offset) / 180.0 * pi;

fChannel = [4, 5, 6];

Fx = V(:,fChannel(1)) * cos(theta(1)) + V(:,fChannel(2)) * cos(theta(2)) + V(:,fChannel(3)) * cos(theta(3));
Fy = V(:,fChannel(1)) * sin(theta(1)) + V(:,fChannel(2)) * sin(theta(2)) + V(:,fChannel(3)) * sin(theta(3));

h = figure;
plot(Fx(1:5000,1));
title('Fx');

h = figure;
plot(Fy(1:5000,1));
title('Fy');