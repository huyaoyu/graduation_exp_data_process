function [loc] = findPeaks(ay,ommit,r)
% ay
% ommit - the firt ommitting number
% r     - the ratio of highest peak and lowest peak
% loc   - cell, the location of peaks, every position is a location vector
%         loc is not shifted by ommit, it is the original location

% find the highest peak

    function [vec_loc] = locatePeaks(vec,level)
        row = size(vec,1);
        vec_loc = 1;
        for J = 2:1:row-1
            if (vec(J,1)   > vec(J-1,1) && ...
                vec(J+1,1) < vec(J,1)   && ...
                vec(J) >= level )
                vec_loc = [vec_loc;J];
            end
        end % I
        
        if (size(vec_loc,1) > 1)
            vec_loc = vec_loc(2:end);
        end
    end % locatePeaks

hp = max(ay(ommit:end,:));

peak_level = hp*r;

col = size(ay,2);

loc   = cell(col,1);

for I = 1:1:col
    vl = locatePeaks(ay(ommit(1)+1:end,I),peak_level(I));
    vl = vl + ommit(1);
    
    loc{I} = vl;
end % I

end % function