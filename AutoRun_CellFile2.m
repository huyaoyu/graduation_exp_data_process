
% automatically handle all the input files

clc
clear
close ALL

filter_flag = 0;

% get ifs variable
input_files_55;

ROTOR_CENTER_COLUMS = [1,2,3,4,5,6,9,10,11,12];
% 808u 806u 807L 804L
% sensor_sensitivity = [1.8935,1.8539,2.5588,2.5889];
% sensor_offset = [-2.8246,-2.7479,-1.5262,-2.1044];
% 808u 806u 014L 804L
% sensor_sensitivity = [1.8935,1.8539,3.182,2.5889];
% sensor_offset = [-2.8246,-2.7479,-0.674,-2.1044];
% 808u 806u 015L 804L
% sensor_sensitivity = [1.8935,1.8539,3.247,2.5889];
% sensor_offset = [-2.8246,-2.7479,-0.5312,-2.1044];
% 808u 806u 016L 804L
sensor_sensitivity = [1.8935,1.8539,2.9519,2.5889];
sensor_offset = [-2.8246,-2.7479,-0.5650,-2.1044];
read_entries = 100000;

scale_matrix = [
    ones(read_entries,1)*sensor_sensitivity(1),...
    ones(read_entries,1)*sensor_sensitivity(2),...
    ones(read_entries,1)*sensor_sensitivity(3),...
    ones(read_entries,1)*sensor_sensitivity(4)
    ];
offset_matrix = [
    ones(read_entries,1)*sensor_offset(1),...
    ones(read_entries,1)*sensor_offset(2),...
    ones(read_entries,1)*sensor_offset(3),...
    ones(read_entries,1)*sensor_offset(4)
    ];

% % get the file name and path name form GUI
% [fn,fp] = uigetfile({'*.lvm;*.dat;*.txt','*.*'},'Selecte a file.');
% 
% if (size(fn,2) == 1 || size(fp,2) == 1)
%     disp('No file specified. Abort from script.');
%     return;
% end
% 
% % analysis the file name
% [pathstr, name, ext, versn] = fileparts([fp,fn]);

% loop for every file
row = size(ifs,1);
fp = 'C:/Users/Sharry/Desktop/20160622����/';
out_prefix = 'C:/Users/Sharry/Desktop/20160622����/output/';

n_rotor_center = size(ROTOR_CENTER_COLUMS,2);
fft_results = zeros(5001,n_rotor_center*row);

for I = 1:1:row
    fn = ifs{I,:};
    % analysis the file name
%     [pathstr, name, ext, versn] = fileparts([fp,fn]);
    [pathstr, name, ext] = fileparts([fp,fn]);
    
    % get data from the file
    disp(['get data from file...',fn]);
    [nc,V] = getData([fp,fn],14,24,read_entries);
    disp(['Channels = ',num2str(nc)]);
    
    % make new directory
    if (~isdir([out_prefix,name]))
        mkdir([out_prefix,name]);
    end
    
    % save the matrix
    save([out_prefix,name,'/V.mat'],'V','-mat');
    
    % rescale ans offet the data

    V(:,ROTOR_CENTER_COLUMS) = ...
        ( V(:,ROTOR_CENTER_COLUMS ) - offset_matrix) ./ scale_matrix;
    
    if (filter_flag)
        d  = fdesign.lowpass('fp,fst,ap,ast',400.0/5000,500.0/5000,0.8,60);
        Hd = design(d,'butter');
        disp('Filtering...');
        V  = filter(Hd,V);
    end
    
    % fft
    [ay,freq,ph] = fftAtFreq(V(1:10:end,:),1000,0,0);
    [loc,loc2]   = findPeaks(ay(:,ROTOR_CENTER_COLUMS),[10,10],0.1);
    % save the fft information
    fft_results(:,(I-1)*n_rotor_center+1:I*n_rotor_center) = ay(:,ROTOR_CENTER_COLUMS);
    
    % draw and save the figures
    [peak_val] = drawFFT([out_prefix,name,'/',name],name,freq,ay(:,ROTOR_CENTER_COLUMS),10,loc,1);
    
    % save the information to the out-put file
    writeOutFile(fullfile([out_prefix,name,'/'],[name, '.out', '']),peak_val,0);
end

save([out_prefix,'fft_results.mat'],'fft_results','-mat');