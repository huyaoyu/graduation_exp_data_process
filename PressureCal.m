% automatically handle all the input files

clc
clear
close ALL

% get ifs variable
input_files_55;

% Define the indeces.
IDX_PressureDrop1 = 13;
IDX_PressureDrop2 = 14;


IDX_CELL = [IDX_PressureDrop1, IDX_PressureDrop2];

read_entries = 50000;

% loop for every file
row  = size(ifs,1);
nIdx = size(IDX_CELL, 1);
fp = 'C:/Users/Sharry/Desktop/20160630����/';
out_prefix = 'C:/Users/Sharry/Desktop/20160630����/output/';

% n_rotor_center = 2;
% fft_results = zeros(25001,n_rotor_center);

for I = 1:1:row
    fn = ifs{I,:};
    % analysis the file name
%     [pathstr, name, ext, versn] = fileparts([fp,fn]);
    [pathstr, name, ext] = fileparts([fp,fn]);
    
    % get data from the file
    disp(['get data from file...',fn]);
    [nc,V] = getData([fp,fn],14,24,read_entries);
    disp(['Channels = ',num2str(nc)]);
    
    % make new directory
    if (~isdir([out_prefix,name]))
        mkdir([out_prefix,name]);
    end
    
    % Loop for every rotor index.

        PressureDrop1 = mean(V(:,13));
        PressureDrop2 = mean(V(:,14));
        
        %    % fft
% %     [ay,freq,ph] = fftAtFreq(V(1:10:end,:),1000,0,0);
%     [ay,freq,ph] = fftAtFreq([fn, ft],10000,0,0);
    
    
    fprintf('file %d, PressureDrop1 = %e, PressureDrop2 = %e\n', I, PressureDrop1, PressureDrop2);
end