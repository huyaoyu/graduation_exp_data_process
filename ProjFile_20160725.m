% Project file.

% Flags.
flag_filter   = 1;
flag_gain     = 1;
flag_fft      = 0;
flag_dry_gain = 1;

% Load specific file according to the flags.
if ( 1 == flag_dry_gain )
    GetGainList_20160725;
end

% Input files.
input_files_20160725;

% Working directory.
fp = 'D:/Projects/Experiment/20160725_ECON/';
out_prefix = 'D:/Projects/Experiment/20160725_ECON/output/';
