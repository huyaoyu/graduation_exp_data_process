function get_file_names(n1,n2)
%

if ( n1 > n2 )
    fprintf('Wrong number. n1 = %d, n2 = %d\n', n1, n2);
    return;
end

for I = n1:1:n2
    fprintf('''%02d-2.lvm'';\n', I);
end