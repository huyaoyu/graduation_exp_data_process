ifs = {
% '01', 28.0, 30.0, 60.0; % 20Hz rotate, 28Hz whirl.
% '02', 26.0, 30.0, 60.0; % 20Hz rotate, 26Hz whirl.
% '03', 24.0, 25.0, 50.0; % 20Hz rotate, 24Hz whirl.
% '04', 22.0, 25.0, 50.0; % 20Hz rotate, 22Hz whirl.
% '05', 20.0, 25.0, 50.0; % 20Hz rotate, 20Hz whirl.
% '06', 18.0, 25.0, 50.0; % 20Hz rotate, 18Hz whirl.
% '07', 16.0, 25.0, 50.0; % 20Hz rotate, 16Hz whirl.
% '08', 14.0, 25.0, 50.0; % 20Hz rotate, 14Hz whirl.
% '09', 12.0, 25.0, 50.0; % 20Hz rotate, 12Hz whirl.
% '10', 10.0, 25.0, 50.0; % 20Hz rotate, 10Hz whirl.
% '11',  8.0, 25.0, 50.0; % 20Hz rotate, 8Hz whirl.
% '12',  6.0, 25.0, 50.0; % 20Hz rotate, 6Hz whirl.
'13', 28.0, 30.0, 60.0; % 26Hz rotate, 28Hz whirl.
'14', 26.0, 30.0, 60.0; % 26Hz rotate, 26Hz whirl.
'15', 24.0, 30.0, 60.0; % 26Hz rotate, 24Hz whirl.
'16', 22.0, 30.0, 60.0; % 26Hz rotate, 22Hz whirl.
'17', 20.0, 30.0, 60.0; % 26Hz rotate, 20Hz whirl.
'18', 18.0, 30.0, 60.0; % 26Hz rotate, 18Hz whirl.
'19', 16.0, 30.0, 60.0; % 26Hz rotate, 16Hz whirl.
'20', 14.0, 30.0, 60.0; % 26Hz rotate, 14Hz whirl.
'21', 12.0, 30.0, 60.0; % 26Hz rotate, 12Hz whirl.
'22', 10.0, 30.0, 60.0; % 26Hz rotate, 10Hz whirl.
'23',  8.0, 30.0, 60.0; % 26Hz rotate, 8Hz whirl.
'24',  6.0, 30.0, 60.0; % 26Hz rotate, 6Hz whirl.
'25', 28.0, 40.0, 80.0; % 34Hz rotate, 28Hz whirl.
'26', 26.0, 40.0, 80.0; % 34Hz rotate, 26Hz whirl.
'27', 24.0, 40.0, 80.0; % 34Hz rotate, 24Hz whirl.
'28', 22.0, 40.0, 80.0; % 34Hz rotate, 22Hz whirl.
'29', 20.0, 40.0, 80.0; % 34Hz rotate, 20Hz whirl.
'30', 18.0, 40.0, 80.0; % 34Hz rotate, 18Hz whirl.
'31', 16.0, 40.0, 80.0; % 34Hz rotate, 16Hz whirl.
'32', 14.0, 40.0, 80.0; % 34Hz rotate, 14Hz whirl.
'33', 12.0, 40.0, 80.0; % 34Hz rotate, 12Hz whirl.
'34', 10.0, 40.0, 80.0; % 34Hz rotate, 10Hz whirl.
'35',  8.0, 40.0, 80.0; % 34Hz rotate, 8Hz whirl.
'36',  6.0, 40.0, 80.0; % 34Hz rotate, 6Hz whirl.
'37', 28.0, 30.0, 60.0; % 20Hz rotate, 28Hz whirl.
'38', 26.0, 30.0, 60.0; % 20Hz rotate, 26Hz whirl.
'39', 24.0, 30.0, 60.0; % 20Hz rotate, 24Hz whirl.
'40', 22.0, 25.0, 50.0; % 20Hz rotate, 22Hz whirl.
% '41', 20.0, 25.0, 50.0; % 20Hz rotate, 20Hz whirl.
'41_1', 20.0, 25.0, 50.0; % 20Hz rotate, 20Hz whirl.
'42', 18.0, 25.0, 50.0; % 20Hz rotate, 18Hz whirl.
'43', 16.0, 25.0, 50.0; % 20Hz rotate, 16Hz whirl.
'44', 14.0, 25.0, 50.0; % 20Hz rotate, 14Hz whirl.
'45', 12.0, 25.0, 50.0; % 20Hz rotate, 12Hz whirl.
'46', 10.0, 25.0, 50.0; % 20Hz rotate, 10Hz whirl.
'47',  8.0, 25.0, 50.0; % 20Hz rotate, 8Hz whirl.
'48',  6.0, 25.0, 50.0; % 20Hz rotate, 6Hz whirl.
'49', 28.0, 30.0, 60.0; % 26Hz rotate, 28Hz whirl.
'50', 26.0, 30.0, 60.0; % 26Hz rotate, 26Hz whirl.
'51', 24.0, 30.0, 60.0; % 26Hz rotate, 24Hz whirl.
'52', 22.0, 30.0, 60.0; % 26Hz rotate, 22Hz whirl.
'53', 20.0, 30.0, 60.0; % 26Hz rotate, 20Hz whirl.
'54', 18.0, 30.0, 60.0; % 26Hz rotate, 18Hz whirl.
'55', 16.0, 30.0, 60.0; % 26Hz rotate, 16Hz whirl.
% '56', 14.0, 30.0, 60.0; % 26Hz rotate, 14Hz whirl.
'56_1', 14.0, 30.0, 60.0; % 26Hz rotate, 14Hz whirl.
'57', 12.0, 30.0, 60.0; % 26Hz rotate, 12Hz whirl.
'58', 10.0, 30.0, 60.0; % 26Hz rotate, 10Hz whirl.
'59',  8.0, 30.0, 60.0; % 26Hz rotate, 8Hz whirl.
'60',  6.0, 30.0, 60.0; % 26Hz rotate, 6Hz whirl.
'61', 28.0, 30.0, 60.0; % 28Hz whirl.
'62', 24.0, 30.0, 60.0; % 24Hz whirl.
'63', 20.0, 25.0, 50.0; % 20Hz whirl.
'64', 16.0, 20.0, 40.0; % 16Hz whirl.
'65', 12.0, 20.0, 40.0; % 12Hz whirl.
'66',  8.0, 15.0, 30.0; % 8Hz whirl.
'67', 28.0, 30.0, 60.0; % 28Hz whirl.
'68', 24.0, 30.0, 60.0; % 24Hz whirl.
'69', 20.0, 25.0, 50.0; % 20Hz whirl.
'70', 16.0, 20.0, 40.0; % 16Hz whirl.
'71', 12.0, 20.0, 40.0; % 12Hz whirl.
'72',  8.0, 15.0, 30.0; % 8Hz whirl.
    };