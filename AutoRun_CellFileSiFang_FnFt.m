
% automatically handle all the input files

clc
clear
close ALL

filter_flag = 1;

% get ifs variable
input_files_20160709;

fs = 10000;

% Define the indeces.
IDX_UPPER_X = 9;
IDX_UPPER_Y = 10;
IDX_LOWER_X = 11;
IDX_LOWER_Y = 12;

IDX_F_UPPER_X = 17;
IDX_F_UPPER_Y = 18;
IDX_F_LOWER_X = 19;
IDX_F_LOWER_Y = 20;

IDX_CELL = {
    [IDX_UPPER_X, IDX_UPPER_Y, IDX_F_UPPER_X, IDX_F_UPPER_Y];
    [IDX_LOWER_X, IDX_LOWER_Y, IDX_F_LOWER_X, IDX_F_LOWER_Y];
    };

read_entries = 50000;

if ( 1 == filter_flag )
    analysisEntries = read_entries / 2;
end

% loop for every file
row  = size(ifs,1);
nIdx = size(IDX_CELL, 1);
fp = 'D:/Projects/Experiment/20160709实验数据/';
out_prefix = 'D:/Projects/Experiment/20160709实验数据/output/';

% n_rotor_center = 2;
% fft_results = zeros(25001,n_rotor_center);

for I = 1:1:row
    fn = ifs{I,1};
    % analysis the file name
%     [pathstr, name, ext, versn] = fileparts([fp,fn]);
    [pathstr, name, ext] = fileparts([fp,fn]);
    
    dbutter = designfilt(...
    'lowpassiir', 'PassbandFrequency', ifs{I,2},...
    'StopbandFrequency', ifs{I,3}, ...
    'PassbandRipple', 1,...
    'StopbandAttenuation', 60,...
    'SampleRate', fs,...
    'DesignMethod', 'butter');
    
    % get data from the file
%     disp(['get data from file...',fn]);
    [nc,V] = getData([fp,fn],14,24,read_entries);
%     disp(['Channels = ',num2str(nc)]);
    
    % make new directory
    if (~isdir([out_prefix,name]))
        mkdir([out_prefix,name]);
    end
    
    % Filter;
    if ( 1 == filter_flag)
        V = filter(dbutter, V);
        V_Analysis = V(end-analysisEntries+1:end, :);
    end
    
    % Loop for every rotor index.
    for J = 1:1:nIdx
        idxArray = IDX_CELL{J,1};
        
        [fn, ft, ang] = calculate_FnFt(V_Analysis(:,idxArray(1)),...
            V_Analysis(:,idxArray(2)), -V_Analysis(:,idxArray(3)), -V_Analysis(:,idxArray(4)));
        
        %    % fft
% %     [ay,freq,ph] = fftAtFreq(V(1:10:end,:),1000,0,0);
%     [ay,freq,ph] = fftAtFreq([fn, ft],10000,0,0);
    
    ampFn = mean(fn);
    ampFt = mean(ft);
    
%     fprintf('file %d, ampFn = %e, ampFt = %e\n', I, ampFn, ampFt);
    fprintf('%e\t%e\t', ampFn, ampFt);
    end % J
    fprintf('\n');
end