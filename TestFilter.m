% Test FFT.
clear
close all;


% Compose signal.
FRQ1 = 10; % Hz.
FRQ2 = 15; % Hz.
fs = 1000;
A1 = 2;
A2 = 1;
phi1 = 0; % Degree.
phi2 = 120; % Degree.

tSpan = 1; % Second.

t = (0:1:(tSpan * fs) ) / fs;
t = t';

ang1 = FRQ1 * 2 * pi * t + phi1 / 180.0 * pi;
ang2 = FRQ2 * 2 * pi * t + phi2 / 180.0 * pi;

s1 = A1 * cos(ang1);
s2 = A2 * cos(ang2);

figure;
plot(t, s1);
hold on
plot(t, s2, 'r');
hold off

figure;
s = s1 + s2;
% plot(t, s);

[ay,freq,ph] = fftAtFreq(s,fs,0,0);

figure;
semilogx(freq, ay);

% Filter

% FilFreq = 15; % Hz.
% FilBase = fs / 2;
% NFilFreq = FilFreq / FilBase;

% [B, A] = butter(10, NFilFreq,'low');
% [B, A] = cheby1(10, 3, NFilFreq);
% s_filtered = filter(B, A, s);


dbutter = designfilt(...
    'lowpassiir', 'PassbandFrequency', 12,...
    'StopbandFrequency', 20, ...
    'PassbandRipple', 1,...
    'StopbandAttenuation', 60,...
    'SampleRate', fs,...
    'DesignMethod', 'butter');

s_filtered = filter(dbutter, s);

figure;
plot(t, s1);
hold on
plot(t, s_filtered, 'r');
hold off
title('s filetered');
