
% automatically handle all the input files

clc
clear
close ALL

filter_flag = 0;

% get ifs variable
input_files_55;

ROTOR_CENTER_COLUMS = [9,10,11,12,17,18,19,20];

% offset = 45;
% theta_1 = ( [30, 150, -90] + offset) / 180.0 * pi;
% theta_2 = ( [30, 150, -90] + offset) / 180.0 * pi;

% fChannel = [4, 5, 6];

sensor_sensitivity = [1,1,1,1,1,1,1,1];
sensor_offset = [0,0,0,0,0,0,0,0];
read_entries = 50000;

scale_matrix = [
    ones(read_entries,1)*sensor_sensitivity(1),...
    ones(read_entries,1)*sensor_sensitivity(2),...
    ones(read_entries,1)*sensor_sensitivity(3),...
    ones(read_entries,1)*sensor_sensitivity(4),...
    ones(read_entries,1)*sensor_sensitivity(5),...
    ones(read_entries,1)*sensor_sensitivity(6),...
    ones(read_entries,1)*sensor_sensitivity(7),...
    ones(read_entries,1)*sensor_sensitivity(8),...
    ];
offset_matrix = [
    ones(read_entries,1)*sensor_offset(1),...
    ones(read_entries,1)*sensor_offset(2),...
    ones(read_entries,1)*sensor_offset(3),...
    ones(read_entries,1)*sensor_offset(4),...
    ones(read_entries,1)*sensor_offset(5),...
    ones(read_entries,1)*sensor_offset(6),...
    ones(read_entries,1)*sensor_offset(7),...
    ones(read_entries,1)*sensor_offset(8),...
    ];

% % get the file name and path name form GUI
% [fn,fp] = uigetfile({'*.lvm;*.dat;*.txt','*.*'},'Selecte a file.');
% 
% if (size(fn,2) == 1 || size(fp,2) == 1)
%     disp('No file specified. Abort from script.');
%     return;
% end
% 
% % analysis the file name
% [pathstr, name, ext, versn] = fileparts([fp,fn]);

% loop for every file
row = size(ifs,1);
fp = 'C:/Users/huyaoyu/Desktop/20160705����/';
out_prefix = 'C:/Users/huyaoyu/Desktop/20160705����/output/';

n_rotor_center = size(ROTOR_CENTER_COLUMS,2);
fft_results = zeros(25001,n_rotor_center*row);

for I = 1:1:row
    fn = ifs{I,:};
    % analysis the file name
%     [pathstr, name, ext, versn] = fileparts([fp,fn]);
    [pathstr, name, ext] = fileparts([fp,fn]);
    
    % get data from the file
    disp(['get data from file...',fn]);
    [nc,V] = getData([fp,fn],14,24,read_entries);
    disp(['Channels = ',num2str(nc)]);
    
    % make new directory
    if (~isdir([out_prefix,name]))
        mkdir([out_prefix,name]);
    end
    
    % rescale ans offet the data

    V(:,ROTOR_CENTER_COLUMS) = ...
        ( V(:,ROTOR_CENTER_COLUMS ) - offset_matrix) ./ scale_matrix;
    
    if (filter_flag)
        d  = fdesign.lowpass('fp,fst,ap,ast',400.0/5000,500.0/5000,0.8,60);
        Hd = design(d,'butter');
        disp('Filtering...');
        V  = filter(Hd,V);
    end
    
    % Calculate average circumferential velocity.
    avgCirVel = sqrt( mean(V(:,15)) * 2 );
    fprintf('avgCirVel = %6f\n', avgCirVel);
end

save([out_prefix,'fft_results.mat'],'fft_results','-mat');