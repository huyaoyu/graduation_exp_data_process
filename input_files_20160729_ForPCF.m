ifs = {
% % '001', 16.0, 20.0, 40.0; % 0Hz rotate, 16Hz whirl.
% % '002', 16.0, 20.0, 40.0; % 0Hz rotate, 10Hz whirl.
% % '003', 16.0, 20.0, 40.0; % 0Hz rotate, 10Hz whirl.
% % '004', 16.0, 20.0, 40.0; % 0Hz rotate, 16Hz whirl.
% % '005', 16.0, 20.0, 40.0; % 0Hz rotate, 10Hz whirl.
% % '006', 16.0, 20.0, 40.0; % 0Hz rotate, 10Hz whirl.
% '010', 28.0, 30.0, 60.0; % 20Hz rotate, 28Hz whirl.
% '011', 26.0, 30.0, 60.0; % 20Hz rotate, 26Hz whirl.
% '012', 24.0, 30.0, 60.0; % 20Hz rotate, 24Hz whirl.
% '013', 22.0, 25.0, 50.0; % 20Hz rotate, 22Hz whirl.
% '014', 20.0, 25.0, 50.0; % 20Hz rotate, 20Hz whirl.
% '015', 18.0, 25.0, 50.0; % 20Hz rotate, 18Hz whirl.
% '016', 16.0, 25.0, 50.0; % 20Hz rotate, 16Hz whirl.
% '017', 14.0, 25.0, 50.0; % 20Hz rotate, 14Hz whirl.
% '018', 12.0, 25.0, 50.0; % 20Hz rotate, 12Hz whirl.
% '019', 10.0, 25.0, 50.0; % 20Hz rotate, 10Hz whirl.
% '020',  8.0, 25.0, 50.0; % 20Hz rotate,  8Hz whirl.
% '021',  6.0, 25.0, 50.0; % 20Hz rotate,  6Hz whirl.
% % '021_1',  6.0, 25.0, 50.0; % 20Hz rotate,  6Hz whirl.
% '022', 28.0, 30.0, 60.0; % 26Hz rotate, 28Hz whirl.
% '023', 26.0, 30.0, 60.0; % 26Hz rotate, 26Hz whirl.
% '024', 24.0, 30.0, 60.0; % 26Hz rotate, 24Hz whirl.
% '025', 22.0, 30.0, 60.0; % 26Hz rotate, 22Hz whirl.
% '026', 20.0, 30.0, 60.0; % 26Hz rotate, 20Hz whirl.
% '027', 18.0, 30.0, 60.0; % 26Hz rotate, 18Hz whirl.
% '028', 16.0, 30.0, 60.0; % 26Hz rotate, 16Hz whirl.
% '029', 14.0, 30.0, 60.0; % 26Hz rotate, 14Hz whirl.
% '030', 12.0, 30.0, 60.0; % 26Hz rotate, 12Hz whirl.
% '031', 10.0, 30.0, 60.0; % 26Hz rotate, 10Hz whirl.
% '032',  8.0, 30.0, 60.0; % 26Hz rotate,  8Hz whirl.
% '033',  6.0, 30.0, 60.0; % 26Hz rotate,  6Hz whirl.
% '034', 28.0, 40.0, 80.0; % 34Hz rotate, 28Hz whirl.
'035', 26.0, 40.0, 80.0; % 34Hz rotate, 26Hz whirl.
% '036', 24.0, 40.0, 80.0; % 34Hz rotate, 24Hz whirl.
% '037', 22.0, 40.0, 80.0; % 34Hz rotate, 22Hz whirl.
% '038', 20.0, 40.0, 80.0; % 34Hz rotate, 20Hz whirl.
% '039', 18.0, 40.0, 80.0; % 34Hz rotate, 18Hz whirl.
% '040', 16.0, 40.0, 80.0; % 34Hz rotate, 16Hz whirl.
% '041', 14.0, 40.0, 80.0; % 34Hz rotate, 14Hz whirl.
% '042', 12.0, 40.0, 80.0; % 34Hz rotate, 12Hz whirl.
% % '042_1', 12.0, 40.0, 80.0; % 34Hz rotate, 12Hz whirl.
% '043', 10.0, 40.0, 80.0; % 34Hz rotate, 10Hz whirl.
% '044',  8.0, 40.0, 80.0; % 34Hz rotate,  8Hz whirl.
% '045',  6.0, 40.0, 80.0; % 34Hz rotate,  6Hz whirl.
% '046', 28.0, 30.0, 60.0; % 20Hz rotate, 28Hz whirl.
% '047', 26.0, 30.0, 60.0; % 20Hz rotate, 26Hz whirl.
% '048', 24.0, 30.0, 60.0; % 20Hz rotate, 24Hz whirl.
% '049', 22.0, 25.0, 50.0; % 20Hz rotate, 22Hz whirl.
% '050', 20.0, 25.0, 50.0; % 20Hz rotate, 20Hz whirl.
% '051', 18.0, 25.0, 50.0; % 20Hz rotate, 18Hz whirl.
% '052', 16.0, 25.0, 50.0; % 20Hz rotate, 16Hz whirl.
% '053', 14.0, 25.0, 50.0; % 20Hz rotate, 14Hz whirl.
% '054', 12.0, 25.0, 50.0; % 20Hz rotate, 12Hz whirl.
% '055', 10.0, 25.0, 50.0; % 20Hz rotate, 10Hz whirl.
% '056',  8.0, 25.0, 50.0; % 20Hz rotate,  8Hz whirl.
% '057',  6.0, 25.0, 50.0; % 20Hz rotate,  6Hz whirl.
% '058', 28.0, 30.0, 60.0; % 26Hz rotate, 28Hz whirl.
% '059', 26.0, 30.0, 60.0; % 26Hz rotate, 26Hz whirl.
% '060', 24.0, 30.0, 60.0; % 26Hz rotate, 24Hz whirl.
% '061', 22.0, 30.0, 60.0; % 26Hz rotate, 22Hz whirl.
% '062', 20.0, 30.0, 60.0; % 26Hz rotate, 20Hz whirl.
% '063', 18.0, 30.0, 60.0; % 26Hz rotate, 18Hz whirl.
% '064', 16.0, 30.0, 60.0; % 26Hz rotate, 16Hz whirl.
% '065', 14.0, 30.0, 60.0; % 26Hz rotate, 14Hz whirl.
% '066', 12.0, 30.0, 60.0; % 26Hz rotate, 12Hz whirl.
% '067', 10.0, 30.0, 60.0; % 26Hz rotate, 10Hz whirl.
% '068',  8.0, 30.0, 60.0; % 26Hz rotate,  8Hz whirl.
% '069',  6.0, 30.0, 60.0; % 26Hz rotate,  6Hz whirl.
% '070', 28.0, 40.0, 80.0; % 34Hz rotate, 28Hz whirl.
% '071', 26.0, 40.0, 80.0; % 34Hz rotate, 26Hz whirl.
% '072', 24.0, 40.0, 80.0; % 34Hz rotate, 24Hz whirl.
% '073', 22.0, 40.0, 80.0; % 34Hz rotate, 22Hz whirl.
% '074', 20.0, 40.0, 80.0; % 34Hz rotate, 20Hz whirl.
% '075', 18.0, 40.0, 80.0; % 34Hz rotate, 18Hz whirl.
% '076', 16.0, 40.0, 80.0; % 34Hz rotate, 16Hz whirl.
% '077', 14.0, 40.0, 80.0; % 34Hz rotate, 14Hz whirl.
% '078', 12.0, 40.0, 80.0; % 34Hz rotate, 12Hz whirl.
% '079', 10.0, 40.0, 80.0; % 34Hz rotate, 10Hz whirl.
% '080',  8.0, 40.0, 80.0; % 34Hz rotate,  8Hz whirl.
% '081',  6.0, 40.0, 80.0; % 34Hz rotate,  6Hz whirl.
% '082', 28.0, 30.0, 60.0; % 20Hz rotate, 28Hz whirl.
% '083', 26.0, 30.0, 60.0; % 20Hz rotate, 26Hz whirl.
% '084', 24.0, 30.0, 60.0; % 20Hz rotate, 24Hz whirl.
% '085', 22.0, 25.0, 50.0; % 20Hz rotate, 22Hz whirl.
% '086', 20.0, 25.0, 50.0; % 20Hz rotate, 20Hz whirl.
% '087', 18.0, 25.0, 50.0; % 20Hz rotate, 18Hz whirl.
% '088', 16.0, 25.0, 50.0; % 20Hz rotate, 16Hz whirl.
% '089', 14.0, 25.0, 50.0; % 20Hz rotate, 14Hz whirl.
% '090', 12.0, 25.0, 50.0; % 20Hz rotate, 12Hz whirl.
% '091', 10.0, 25.0, 50.0; % 20Hz rotate, 10Hz whirl.
% '092',  8.0, 25.0, 50.0; % 20Hz rotate,  8Hz whirl.
% '093',  6.0, 25.0, 50.0; % 20Hz rotate,  6Hz whirl.
% '094', 28.0, 30.0, 60.0; % 26Hz rotate, 28Hz whirl.
% '095', 26.0, 30.0, 60.0; % 26Hz rotate, 26Hz whirl.
% '096', 24.0, 30.0, 60.0; % 26Hz rotate, 24Hz whirl.
% '097', 22.0, 30.0, 60.0; % 26Hz rotate, 22Hz whirl.
% '098', 20.0, 30.0, 60.0; % 26Hz rotate, 20Hz whirl.
% '099', 18.0, 30.0, 60.0; % 26Hz rotate, 18Hz whirl.
% '100', 16.0, 30.0, 60.0; % 26Hz rotate, 16Hz whirl.
% '101', 14.0, 30.0, 60.0; % 26Hz rotate, 14Hz whirl.
% '102', 12.0, 30.0, 60.0; % 26Hz rotate, 12Hz whirl.
% '103', 10.0, 30.0, 60.0; % 26Hz rotate, 10Hz whirl.
% '104',  8.0, 30.0, 60.0; % 26Hz rotate,  8Hz whirl.
% '105',  6.0, 30.0, 60.0; % 26Hz rotate,  6Hz whirl.
% '106', 28.0, 40.0, 80.0; % 34Hz rotate, 28Hz whirl.
% '107', 26.0, 40.0, 80.0; % 34Hz rotate, 26Hz whirl.
% '108', 24.0, 40.0, 80.0; % 34Hz rotate, 24Hz whirl.
% '109', 22.0, 40.0, 80.0; % 34Hz rotate, 22Hz whirl.
% '110', 20.0, 40.0, 80.0; % 34Hz rotate, 20Hz whirl.
% '111', 18.0, 40.0, 80.0; % 34Hz rotate, 18Hz whirl.
% '112', 16.0, 40.0, 80.0; % 34Hz rotate, 16Hz whirl.
% '113', 14.0, 40.0, 80.0; % 34Hz rotate, 14Hz whirl.
% '114', 12.0, 40.0, 80.0; % 34Hz rotate, 12Hz whirl.
% '115', 10.0, 40.0, 80.0; % 34Hz rotate, 10Hz whirl.
% '116',  8.0, 40.0, 80.0; % 34Hz rotate,  8Hz whirl.
% '117',  6.0, 40.0, 80.0; % 34Hz rotate,  6Hz whirl.
    };