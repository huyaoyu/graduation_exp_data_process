function writeOutFile(fn,peak_val,flag)
% flag - if 0 does not display on the screen

fid = fopen(fn,'w');

n = size(peak_val,1);

for I = 1:1:n
    str = sprintf('vector %d',I);
    fprintf(fid,'%s\n',str);
    if (flag)
        disp(str);
    end
    
    peak_vector = peak_val{I,1};
    
    n_p = size(peak_vector,1);
    
    for J = 1:1:n_p
        str = sprintf('%2d: f = %5.1fHz, Amp = %.5f.',J,...
            peak_vector(J,1),peak_vector(J,2));
        fprintf(fid,'%s\n',str);
        if (flag)
            disp(str);
        end
    end % J
    
    str = sprintf('\n');
    fprintf(fid,'%s\n',str);
    
    if (flag)
        disp(str);
    end
end % I

fclose(fid);

end % function