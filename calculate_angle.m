function [ang] = calculate_angle(x, y)
% Calculate the angle of points (x,y)
% x,y - column vectors.
% ang - the angle relatvie to x, rad

% Constants.

SMALL_VALUE = 1e-20;

% Dimension check.

[rowX, colX] = size(x);
[rowY, colY] = size(y);

if ( rowX ~= rowY || colX ~= 1 || colY ~= 1 )
    fprintf('Wrong dimension. x(%d, %d), y(%d, %d)\n',...
        rowX, colX, rowY, colY);
    
    ang = 0;
    return;
end

ang = zeros(rowX, 1);

for I = 1:1:rowX
    currentX = x(I);
    currentY = y(I);
    
    if ( abs(currentX) <= SMALL_VALUE)
        if (abs(currentY) <= SMALL_VALUE)
            fprintf('Too small value. x(%d) = %e, y(%d) = %e\n', I, currentX, I, currentY);
            ang(I) = 0;
            continue;
        end

        if ( currentY > 0 )
            ang(I) = pi/2;
        else
            ang(I) = -pi/2;
        end

        continue;
    end
    
    t = currentY / currentX;
    
    tempAng = atan(t);
    
    if ( currentX < 0 )
        if ( currentY > 0 )
            tempAng = tempAng + pi;
        else
            tempAng = tempAng - pi;
        end
    end
    
    ang(I) = tempAng;
    
end % I


