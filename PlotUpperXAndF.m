
close all

CH = [1; 2; 3; 4; 5; 6; 7; 8];
titleCell = {
    'CH01';
    'CH02';
    'CH03';
    'CH04';
    'CH05';
    'CH06';
    'CH07';
    'CH08';
    };

for I = 1:1:8
    figure;
    plot((V_Scaled(:,9) + 2.55) * 200);
    hold on
    plot(V_Scaled(:,CH(I,1)), 'r');
    hold off
    title(titleCell{I,1});
end % I

