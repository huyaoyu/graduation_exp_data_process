clear;

rho = 997.4;

R1 = 0.29;
C  = 0.005;
R2 = R1 + C;
S  = pi * (R2^2 - R1^2);
M  = 10.975;
OMEGA = 188.5;

Va = M/rho/S;
Vt = R1 * OMEGA;
B = Va / Vt;
fprintf('Va = %f, B = %f\n', Va, B);

r1 = 0.05;
c  = 0.001;
r2 = r1 + c;
s  = pi * (r2^2 - r1^2);
omega = 34 * 2 * pi;
vt = omega * r1;
va = vt * B;
mv = va * s * 3600;
fprintf('va = %f, mv = %f\n', va, mv);

