ifs = {
'001', 28.0, 45.0, 90.0; % 40Hz rotate, 28Hz whirl.
'002', 26.0, 45.0, 90.0; % 40Hz rotate, 26Hz whirl.
'003', 24.0, 45.0, 90.0; % 40Hz rotate, 24Hz whirl.
'004', 22.0, 45.0, 90.0; % 40Hz rotate, 22Hz whirl.
'005', 20.0, 45.0, 90.0; % 40Hz rotate, 20Hz whirl.
'006', 18.0, 45.0, 90.0; % 40Hz rotate, 18Hz whirl.
'007', 16.0, 45.0, 90.0; % 40Hz rotate, 16Hz whirl.
'008', 14.0, 45.0, 90.0; % 40Hz rotate, 14Hz whirl.
'009', 12.0, 45.0, 90.0; % 40Hz rotate, 12Hz whirl.
'010', 10.0, 45.0, 90.0; % 40Hz rotate, 10Hz whirl.
'011',  8.0, 45.0, 90.0; % 40Hz rotate,  8Hz whirl.
'012',  6.0, 45.0, 90.0; % 40Hz rotate,  6Hz whirl.
% '013',  0.0, 45.0, 90.0; % 40Hz rotate,  0Hz whirl.
'014', 28.0, 45.0, 90.0; % 40Hz rotate, 28Hz whirl.
'015', 26.0, 45.0, 90.0; % 40Hz rotate, 26Hz whirl.
'016', 24.0, 45.0, 90.0; % 40Hz rotate, 24Hz whirl.
'017', 22.0, 45.0, 90.0; % 40Hz rotate, 22Hz whirl.
'018', 20.0, 45.0, 90.0; % 40Hz rotate, 20Hz whirl.
'019', 18.0, 45.0, 90.0; % 40Hz rotate, 18Hz whirl.
'020', 16.0, 45.0, 90.0; % 40Hz rotate, 16Hz whirl.
'021', 14.0, 45.0, 90.0; % 40Hz rotate, 14Hz whirl.
'022', 12.0, 45.0, 90.0; % 40Hz rotate, 12Hz whirl.
'023', 10.0, 45.0, 90.0; % 40Hz rotate, 10Hz whirl.
'024',  8.0, 45.0, 90.0; % 40Hz rotate,  8Hz whirl.
'025',  6.0, 45.0, 90.0; % 40Hz rotate,  6Hz whirl.
% '026',  0.0, 45.0, 90.0; % 40Hz rotate,  0Hz whirl.
'027', 28.0, 45.0, 90.0; % 40Hz rotate, 28Hz whirl.
'028', 26.0, 45.0, 90.0; % 40Hz rotate, 26Hz whirl.
'029', 24.0, 45.0, 90.0; % 40Hz rotate, 24Hz whirl.
'030', 22.0, 45.0, 90.0; % 40Hz rotate, 22Hz whirl.
'031', 20.0, 45.0, 90.0; % 40Hz rotate, 20Hz whirl.
'032', 18.0, 45.0, 90.0; % 40Hz rotate, 18Hz whirl.
'033', 16.0, 45.0, 90.0; % 40Hz rotate, 16Hz whirl.
'034', 14.0, 45.0, 90.0; % 40Hz rotate, 14Hz whirl.
'035', 12.0, 45.0, 90.0; % 40Hz rotate, 12Hz whirl.
'036', 10.0, 45.0, 90.0; % 40Hz rotate, 10Hz whirl.
'037',  8.0, 45.0, 90.0; % 40Hz rotate,  8Hz whirl.
'038',  6.0, 45.0, 90.0; % 40Hz rotate,  6Hz whirl.
% '039',  0.0, 45.0, 90.0; % 40Hz rotate,  0Hz whirl.
'040',  30.0, 45.0, 90.0; % 40Hz rotate,  30Hz whirl.
'041',  30.0, 45.0, 90.0; % 40Hz rotate,  30Hz whirl.
'042',  30.0, 45.0, 90.0; % 40Hz rotate,  30Hz whirl.
'043',  30.0, 40.0, 80.0; % 34Hz rotate,  30Hz whirl.
'044',  30.0, 40.0, 80.0; % 34Hz rotate,  30Hz whirl.
'045',  30.0, 40.0, 80.0; % 34Hz rotate,  30Hz whirl.
'046',  30.0, 35.0, 70.0; % 26Hz rotate,  30Hz whirl.
'047',  30.0, 35.0, 70.0; % 26Hz rotate,  30Hz whirl.
'048',  30.0, 35.0, 70.0; % 26Hz rotate,  30Hz whirl.
'049',  30.0, 35.0, 70.0; % 20Hz rotate,  30Hz whirl.
'050',  30.0, 35.0, 70.0; % 20Hz rotate,  30Hz whirl.
'051',  30.0, 35.0, 70.0; % 20Hz rotate,  30Hz whirl.
% '052',  00.0, 35.0, 70.0; % 34Hz rotate,  0Hz whirl.
% '053',  00.0, 35.0, 70.0; % 34Hz rotate,  0Hz whirl.
% '054',  00.0, 35.0, 70.0; % 34Hz rotate,  0Hz whirl.
% '055',  00.0, 35.0, 70.0; % 26Hz rotate,  0Hz whirl.
% '056',  00.0, 35.0, 70.0; % 26Hz rotate,  0Hz whirl.
% '057',  00.0, 35.0, 70.0; % 26Hz rotate,  0Hz whirl.
% '058',  00.0, 35.0, 70.0; % 20Hz rotate,  0Hz whirl.
% '059',  00.0, 35.0, 70.0; % 20Hz rotate,  0Hz whirl.
% '060',  00.0, 35.0, 70.0; % 20Hz rotate,  0Hz whirl.
% '061',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '062',  38.0, 40.0, 80.0; % 38Hz rotate,  00Hz whirl.
% '063',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '064',  34.0, 40.0, 80.0; % 34Hz rotate,  00Hz whirl.
% '065',  32.0, 35.0, 70.0; % 32Hz rotate,  00Hz whirl.
% '066',  30.0, 35.0, 70.0; % 30Hz rotate,  00Hz whirl.
% '067',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '068',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '069',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '070',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '071',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '072',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '073',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '074',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '075',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '076',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '077',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '078',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '079',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '080',  38.0, 40.0, 80.0; % 38Hz rotate,  00Hz whirl.
% '081',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '082',  34.0, 40.0, 80.0; % 34Hz rotate,  00Hz whirl.
% '083',  32.0, 35.0, 70.0; % 32Hz rotate,  00Hz whirl.
% '084',  30.0, 35.0, 70.0; % 30Hz rotate,  00Hz whirl.
% '085',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '086',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '087',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '088',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '089',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '090',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '091',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '092',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '093',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '094',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '095',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '096',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '097',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '098',  38.0, 40.0, 80.0; % 38Hz rotate,  00Hz whirl.
% '099',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '100',  34.0, 40.0, 80.0; % 34Hz rotate,  00Hz whirl.
% '101',  32.0, 35.0, 70.0; % 32Hz rotate,  00Hz whirl.
% '102',  30.0, 35.0, 70.0; % 30Hz rotate,  00Hz whirl.
% '103',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '104',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '105',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '106',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '107',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '108',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '109',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '110',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '111',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '112',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '113',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '114',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '115',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '116',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '117',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '118',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '119',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '120',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '121',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '122',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '123',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '124',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '125',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '126',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '127',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '128',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '129',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '130',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '131',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '132',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '133',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '134',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '135',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '136',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '137',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '138',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '139',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '140',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '141',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '142',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '143',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '144',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '145',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '146',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '147',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '148',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '149',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '150',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '151',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '152',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '153',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '154',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '155',  32.0, 40.0, 80.0; % 32Hz rotate,  00Hz whirl.
% '156',  32.0, 40.0, 80.0; % 32Hz rotate,  00Hz whirl.
% '157',  28.0, 30.0, 60.0; % 28Hz rotate,  00Hz whirl.
% '158',  28.0, 30.0, 60.0; % 28Hz rotate,  00Hz whirl.
% '159',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '160',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '161',  20.0, 25.0, 50.0; % 20Hz rotate,  00Hz whirl.
% '162',  20.0, 25.0, 50.0; % 20Hz rotate,  00Hz whirl.
% '163',  16.0, 20.0, 40.0; % 16Hz rotate,  00Hz whirl.
% '164',  16.0, 20.0, 40.0; % 16Hz rotate,  00Hz whirl.
% '165',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '166',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '167',   8.0, 15.0, 30.0; %  8Hz rotate,  00Hz whirl.
% '168',   8.0, 15.0, 30.0; %  8Hz rotate,  00Hz whirl.
% '169',   0.0, 15.0, 30.0; %  0Hz rotate,  00Hz whirl.
% '170',   0.0, 15.0, 30.0; %  0Hz rotate,  00Hz whirl.
% '171',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '172',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '173',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '174',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '175',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '176',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '177',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '178',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '179',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '180',   34, 40.0, 80.0; %  34Hz rotate,  00Hz whirl.
% '901', 40, 45, 90; % Stop.
% '902', 40, 45, 90; % Stop.
% '903', 40, 45, 90; % Stop.
    };