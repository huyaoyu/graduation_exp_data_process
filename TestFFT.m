% Test FFT.
clear
close all;


% Compose signal.
FRQ1 = 10; % Hz.
FRQ2 = 20; % Hz.
fs = 1000;
A1 = 1;
A2 = 2;
phi1 = 0; % Degree.
phi2 = 120; % Degree.

tSpan = 1; % Second.

t = (0:1:(tSpan * fs) ) / fs;
t = t';

ang1 = FRQ1 * 2 * pi * t + phi1 / 180.0 * pi;
ang2 = FRQ2 * 2 * pi * t + phi2 / 180.0 * pi;

s1 = A1 * cos(ang1);
s2 = A2 * cos(ang2);

figure;
plot(t, s1);
hold on
plot(t, s2, 'r');
hold off

figure;
s = s1 + s2;
plot(t, s);

[ay,freq,ph] = fftAtFreq(s,fs,0,0);

figure;
semilogx(freq, ay);
