
% automatically handle all the input files

clc
clear
close ALL

filter_flag = 1;

ProjFile_20160802;

CHANNEL_FILE_PREFIX = 'Data_Chan';
LINE_START = 15;

fs = SAMPLE_FREQ;
% FilFreq = 40; % Hz.
% FilBase = fs / 2;
% NFilFreq = FilFreq / FilBase;
% 
% [B, A] = butter(15, NFilFreq,'low');

% dbutter = designfilt(...
%     'lowpassiir', 'PassbandFrequency', 30,...
%     'StopbandFrequency', 100, ...
%     'PassbandRipple', 1,...
%     'StopbandAttenuation', 60,...
%     'SampleRate', fs,...
%     'DesignMethod', 'butter');

ROTOR_CENTER_COLUMS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
% ROTOR_CENTER_COLUMS = [1,2];

FFT_COLUMS = [9, 10, 11, 12];
N_FFT_RESULTS = 8;

offset = 45;
theta_1 = ( [ 90, -150, -30, -90] + offset) / 180.0 * pi;
theta_2 = ( [ 30,  150, -90,  90] + offset) / 180.0 * pi;

fChannels_1 = [1, 2, 3, 7];
fChannels_2 = [4, 5, 6, 8];

nFCh = size(fChannels_1, 2);

% NOTE: The data of the displacement probes should be multiplied by -1.
S_F_01 = 207.555;
S_F_05 = 187.899;
S_F_06 = 193.573;
S_F_09 = 200.562;
S_F_12 = 193.013;
S_F_08 = 190.150;
S_F_13 = 200.321;
S_F_15 = 198.295;

S_D_94 = 5.422524e-01;
S_D_95 = 5.532162e-01;
S_D_96 = 5.569130e-01;
S_D_97 = 5.490914e-01;

S_D_94 = S_D_94 * -1;
S_D_95 = S_D_95 * -1;
S_D_96 = S_D_96 * -1;
S_D_97 = S_D_97 * -1;

B_D_94 = -5.773662e-01;
B_D_95 = -5.804904e-01;
B_D_96 = -5.653657e-01;
B_D_97 = -5.673390e-01;

B_D_94 = B_D_94 * -1;
B_D_95 = B_D_95 * -1;
B_D_96 = B_D_96 * -1;
B_D_97 = B_D_97 * -1;


sensor_sensitivity = [
    S_F_06, S_F_05, S_F_01, ...
    S_F_09, S_F_12, S_F_08, ...
    S_F_13, S_F_15,...
    S_D_95, S_D_94,...
    S_D_96, S_D_97];
sensor_offset      = [
    0, 0, 0,...
    0, 0, 0,...
    0, 0,...
    B_D_95, B_D_94,...
    B_D_96, B_D_97];

read_entries       = fs * 10; % NOTE: Should be an even number.

if ( 1 == filter_flag )
    nFFTEntries = 0.8 * read_entries;
else
    nFFTEntries = read_entries;
end

cos_matrix_cell = {
    ones(read_entries,1) * cos(theta_1);
    ones(read_entries,1) * cos(theta_2);
    };
sin_matrix_cell = {
    ones(read_entries,1) * sin(theta_1);
    ones(read_entries,1) * sin(theta_2);
    };

scale_matrix = [
    ones(read_entries,1)*sensor_sensitivity(1),...
    ones(read_entries,1)*sensor_sensitivity(2),...
    ones(read_entries,1)*sensor_sensitivity(3),...
    ones(read_entries,1)*sensor_sensitivity(4),...
    ones(read_entries,1)*sensor_sensitivity(5),...
    ones(read_entries,1)*sensor_sensitivity(6),...
    ones(read_entries,1)*sensor_sensitivity(7),...
    ones(read_entries,1)*sensor_sensitivity(8),...
    ones(read_entries,1)*sensor_sensitivity(9),...
    ones(read_entries,1)*sensor_sensitivity(10),...
    ones(read_entries,1)*sensor_sensitivity(11),...
    ones(read_entries,1)*sensor_sensitivity(12)
    ];
offset_matrix = [
    ones(read_entries,1)*sensor_offset(1),...
    ones(read_entries,1)*sensor_offset(2),...
    ones(read_entries,1)*sensor_offset(3),...
    ones(read_entries,1)*sensor_offset(4),...
    ones(read_entries,1)*sensor_offset(5),...
    ones(read_entries,1)*sensor_offset(6),...
    ones(read_entries,1)*sensor_offset(7),...
    ones(read_entries,1)*sensor_offset(8),...
    ones(read_entries,1)*sensor_offset(9),...
    ones(read_entries,1)*sensor_offset(10),...
    ones(read_entries,1)*sensor_offset(11),...
    ones(read_entries,1)*sensor_offset(12)
    ];

% % get the file name and path name form GUI
% [fn,fp] = uigetfile({'*.lvm;*.dat;*.txt','*.*'},'Selecte a file.');
% 
% if (size(fn,2) == 1 || size(fp,2) == 1)
%     disp('No file specified. Abort from script.');
%     return;
% end
% 
% % analysis the file name
% [pathstr, name, ext, versn] = fileparts([fp,fn]);

% loop for every file
row = size(ifs,1);

n_rotor_center = size(ROTOR_CENTER_COLUMS,2);
fft_results = zeros(nFFTEntries/2 + 1,N_FFT_RESULTS*row);

for I = 1:1:row
    fn = ifs{I,1};
    % analysis the file name
%     [pathstr, name, ext, versn] = fileparts([fp,fn]);
%     [pathstr, name, ext] = fileparts([fp,fn]);
    name = fn;
    
    dbutter = designfilt(...
    'lowpassiir', 'PassbandFrequency', ifs{I,2},...
    'StopbandFrequency', ifs{I,3}, ...
    'PassbandRipple', 1,...
    'StopbandAttenuation', 60,...
    'SampleRate', fs,...
    'DesignMethod', 'butter');
    
    % get data from the file
    disp(['get data from file...',fn]);
%     [nc,V] = getData([fp,fn],14,24,read_entries);
%     disp(['Channels = ',num2str(nc)]);
    
    [V] = get_data_ECON([fp,fn], CHANNEL_FILE_PREFIX, LINE_START, read_entries, n_rotor_center);
    
    % make new directory
    if (~isdir([out_prefix,name]))
        mkdir([out_prefix,name]);
    end
    
    % save the matrix
    save([out_prefix,name,'/V.mat'],'V','-mat');
    
    % rescale ans offet the data

    V_Scaled = ...
         V .* scale_matrix - offset_matrix;
     
    if (filter_flag)
        V_Scaled = filter(dbutter, V_Scaled);
    end
    
    % Calculate fx and fy.
    fProjected = V_Scaled(:, fChannels_1) .* cos_matrix_cell{1};
    fx1 = sum(fProjected, 2);
    
    fProjected = V_Scaled(:, fChannels_1) .* sin_matrix_cell{1};
    fy1 = sum(fProjected, 2);
    
    fProjected = V_Scaled(:, fChannels_2) .* cos_matrix_cell{2};
    fx2 = sum(fProjected, 2);
    
    fProjected = V_Scaled(:, fChannels_2) .* sin_matrix_cell{2};
    fy2 = sum(fProjected, 2);
    
    
    VV = [V_Scaled(:, FFT_COLUMS), fx1, fy1, fx2, fy2];
    VV = VV(end-nFFTEntries+1:end, :);
    
    % fft
%     [ay,freq,ph] = fftAtFreq(V(1:10:end,:),1000,0,0);
    [ay,freq,ph] = fftAtFreq(VV,SAMPLE_FREQ,0,0);
    [loc]   = findPeaks(ay,10,PEAK_RATIO);
    % save the fft information
    fft_results(:,(I-1)*N_FFT_RESULTS+1:I*N_FFT_RESULTS) = ay;
    
    % draw and save the figures
    [peak_val] = drawFFT([out_prefix,name,'/',name],name,freq,ay,10,loc,1);
    
    % save the information to the out-put file
    writeOutFile(fullfile([out_prefix,name,'/'],[name, '.out', '']),peak_val,0);
end

save([out_prefix,'fft_results.mat'],'fft_results','-mat');