function [peak_val] = drawFFT(fn,name,freq,ay,ommit,peak_loc,flag)
% Draw several semilogx plots for the FFT results stored in ay.
% freq and ay have the same rows.
% ommit means how many entries should be ommitted while
% plotting the resluts to avoid a large mean value
% peak_loc is a cell. Every element of the cell is a vector
% storing the peak locations.
% peak_val is a cell, every element of the cell is matrix representing
% the frequency and amplitute pair of the specified ay argument.
% fn is the file name and path withou the extention.
% flag - if 1, deletes the figure after saving, if 0 does not delete.
col = size(ay,2);

peak_val = cell(col,1);

temp_peak_val = 0;

for I = 1:1:col
    disp(['Composing the ',num2str(I),'th figure...']);
    h = figure;

    semilogx(freq(ommit+1:1:end),ay(ommit+1:1:end,I));
    
    % title
    title([name,' vetor ',num2str(I)]);
    xlabel('frequency(Hz)');
    ylabel('amplitute(mm)');
    
    hold on
    pl = peak_loc{I};
    n_pl = size(pl,1);
    
    temp_peak_val = zeros(n_pl,2);
    
    for J = 1:1:n_pl
        x = freq(pl(J));
        y = ay(pl(J),I);
        text(x,y,num2str(J));
        temp_peak_val(J,:) = [x,y];
        semilogx(x,y,'ro');
    end % J
    hold off
    
    peak_val{I,1} = temp_peak_val;
    
    % save the figure
    figure_file_name = [fn,'-',num2str(I),'.fig'];
    saveas(h,figure_file_name);
    
    figure_file_name = [fn,'-',num2str(I),'.png'];
    saveas(h,figure_file_name);
    
    if (flag)
        close(h);
    end
end % I

end % function