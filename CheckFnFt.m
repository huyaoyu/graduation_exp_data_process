close all;

% Plot fn and ft.

fStart = 1280;
fEnd   = 1450;
fScale = 1.0;

disScale = 300;

figure;
plot(fn(fStart:fEnd,1));
title('fn and ft');

hold on;
plot(ft(fStart:fEnd,1), 'r');
hold off

% Plot the trajactory.
meanX = mean(VV(:, IDX_LOWER_X));
meanY = mean(VV(:, IDX_LOWER_Y));
x = VV(:, IDX_LOWER_X) - meanX;
y = VV(:, IDX_LOWER_Y) - meanY;
fx = -VV(:, IDX_F_LOWER_X);
fy = -VV(:, IDX_F_LOWER_Y);

% [ang1] = calculate_angle(x, y);

% meanX = mean(VV(:, IDX_UPPER_X));
% meanY = mean(VV(:, IDX_UPPER_Y));
% x = VV(:, IDX_UPPER_X) - meanX;
% y = VV(:, IDX_UPPER_Y) - meanY;
% fx = -VV(:, IDX_F_UPPER_X);
% fy = -VV(:, IDX_F_UPPER_Y);

figure;
plot(x(fStart:fEnd,1), y(fStart:fEnd,1),'-*');
title('trajactory');

% Plot the displacement.
figure;
plot(x(fStart:fEnd,1)*disScale);
hold on
plot(fx(fStart:fEnd,1), 'r');
hold off
title('x and fx');

% Plot the angle.

figure;
plot(ang(fStart:fEnd,1));
title('angle');

% Plot reconstructed trajactory.
maxX = max(x);
maxY = max(y);
xr = maxX * cos(ang);
yr = maxX * sin(ang);
figure;
plot(xr(fStart:fEnd,1), yr(fStart:fEnd,1));
title('Reconstructed trajacotry');

% Quiver plot.
figure;
quiver(x(fStart:fEnd,1), y(fStart:fEnd,1), fx(fStart:fEnd,1)*fScale, fy(fStart:fEnd,1)*fScale);
title('force on rotor');
grid on

