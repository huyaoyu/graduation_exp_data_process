% Project file.

% Flags.
flag_filter   = 1;
flag_gain     = 1;
flag_fft      = 1;
flag_dry_gain = 1;

% SAMPLE_FREQ = 1280; % Hz.
SAMPLE_FREQ = 2560; % Hz.
PEAK_RATIO = 0.1;

% Load specific file according to the flags.
if ( 1 == flag_dry_gain )
%     GetGainList_20160729;
    GetGainList_20160729_new_ForPCF;
end

% Input files.
input_files_20160729_ForPCF;

% Working directory.
% fp         = 'D:/Projects/Experiment/20160729_ECON/';
% out_prefix = 'D:/Projects/Experiment/20160729_ECON/output/';
fp         = 'E:/Experiments/Graduate/20160729_ECON/';
out_prefix = 'E:/Experiments/Graduate/20160729_ECON/output/';

