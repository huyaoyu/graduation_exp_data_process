

figure;
plot(VV(:,1) - mean(VV(:,1)))
hold on
plot(VV(:,3) - mean(VV(:,3)), 'r')
hold off

figure;
plot(VV(:,2) - mean(VV(:,2)))
hold on
plot(VV(:,4) - mean(VV(:,4)), 'r')
hold off

spanUpperX = max(VV(:,1)) - min(VV(:,1));
spanUpperY = max(VV(:,2)) - min(VV(:,2));
spanLowerX = max(VV(:,3)) - min(VV(:,3));
spanLowerY = max(VV(:,4)) - min(VV(:,4));

fprintf('span: %f, %f, %f, %f', spanUpperX, spanUpperY, spanLowerX, spanLowerY);

