
% automatically handle all the input files

clc
clear
close ALL

% Default flags.
flag_filter   = 1;
flag_gain     = 1;
flag_fft      = 0;
flag_dry_gain = 0;
flag_ucs      = 1; % Uncertainty Calculation Struct.

% if ( 1 == flag_dry_gain )
%     GetGainList_20160725;
% end
% 
% % get ifs variable
% input_files_20160725;
% 
% % fp = 'D:/Projects/Experiment/20160716_ECON/';
% % out_prefix = 'D:/Projects/Experiment/20160716_ECON/output/';
% 
% fp = 'D:/Projects/Experiment/20160725_ECON/';
% out_prefix = 'D:/Projects/Experiment/20160725_ECON/output/';

% Load project file.
% ProjFile_20160715
% ProjFile_20160716
% ProjFile_20160729
% ProjFile_20160730
run ./ProjectFiles/20160731/ProjFile_20160731

% ProjFile_20160729_ForPCF;
% ProjFile_20160731_ForPCF;

% ProjFile_20160802

CHANNEL_FILE_PREFIX = 'Data_Chan';
LINE_START = 15;

fs = SAMPLE_FREQ;
% FilFreq = 20; % Hz.
% FilBase = fs / 2;
% NFilFreq = FilFreq / FilBase;
% 
% % [B, A, k] = butter(30, NFilFreq,'low');
% [B, A] = cheby1(4, 3, NFilFreq);

ROTOR_CENTER_COLUMS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
% ROTOR_CENTER_COLUMS = [1,2];

FFT_COLUMS = [9, 10, 11, 12];
DISPLACEMENT_SCALE = 0.001;
N_FFT_RESULTS = 8;

offset = 45;
theta_1 = ( [ 90, -150, -30, -90] + offset) / 180.0 * pi;
theta_2 = ( [ 30,  150, -90,  90] + offset) / 180.0 * pi;

fChannels_1 = [1, 2, 3, 7];
fChannels_2 = [4, 5, 6, 8];

nFCh = size(fChannels_1, 2);

% NOTE: The data of the displacement probes should be multiplied by -1.
S_F_01 = 207.555;
S_F_05 = 187.899;
S_F_06 = 193.573;
S_F_09 = 200.562;
S_F_12 = 193.013;
S_F_08 = 190.150;
S_F_13 = 200.321;
S_F_15 = 198.295;

S_D_94 = 5.422524e-01;
S_D_95 = 5.532162e-01;
S_D_96 = 5.569130e-01;
S_D_97 = 5.490914e-01;

S_D_94 = S_D_94 * -1;
S_D_95 = S_D_95 * -1;
S_D_96 = S_D_96 * -1;
S_D_97 = S_D_97 * -1;

B_D_94 = -5.773662e-01;
B_D_95 = -5.804904e-01;
B_D_96 = -5.653657e-01;
B_D_97 = -5.673390e-01;

B_D_94 = B_D_94 * -1;
B_D_95 = B_D_95 * -1;
B_D_96 = B_D_96 * -1;
B_D_97 = B_D_97 * -1;


sensor_sensitivity = [
    S_F_06, S_F_05, S_F_01, ...
    S_F_09, S_F_12, S_F_08, ...
    S_F_13, S_F_15,...
    S_D_95, S_D_94,...
    S_D_96, S_D_97];
sensor_offset      = [
    0, 0, 0,...
    0, 0, 0,...
    0, 0,...
    B_D_95, B_D_94,...
    B_D_96, B_D_97];
read_entries       = fs * 8; % NOTE: Should be an even number.

if ( 1 == flag_filter )
    analysisEntries = read_entries / 2;
end

cos_matrix_cell = {
    ones(analysisEntries,1) * cos(theta_1);
    ones(analysisEntries,1) * cos(theta_2);
    };
sin_matrix_cell = {
    ones(analysisEntries,1) * sin(theta_1);
    ones(analysisEntries,1) * sin(theta_2);
    };

scale_matrix = [
    ones(read_entries,1)*sensor_sensitivity(1),...
    ones(read_entries,1)*sensor_sensitivity(2),...
    ones(read_entries,1)*sensor_sensitivity(3),...
    ones(read_entries,1)*sensor_sensitivity(4),...
    ones(read_entries,1)*sensor_sensitivity(5),...
    ones(read_entries,1)*sensor_sensitivity(6),...
    ones(read_entries,1)*sensor_sensitivity(7),...
    ones(read_entries,1)*sensor_sensitivity(8),...
    ones(read_entries,1)*sensor_sensitivity(9),...
    ones(read_entries,1)*sensor_sensitivity(10),...
    ones(read_entries,1)*sensor_sensitivity(11),...
    ones(read_entries,1)*sensor_sensitivity(12)
    ];
offset_matrix = [
    ones(read_entries,1)*sensor_offset(1),...
    ones(read_entries,1)*sensor_offset(2),...
    ones(read_entries,1)*sensor_offset(3),...
    ones(read_entries,1)*sensor_offset(4),...
    ones(read_entries,1)*sensor_offset(5),...
    ones(read_entries,1)*sensor_offset(6),...
    ones(read_entries,1)*sensor_offset(7),...
    ones(read_entries,1)*sensor_offset(8),...
    ones(read_entries,1)*sensor_offset(9),...
    ones(read_entries,1)*sensor_offset(10),...
    ones(read_entries,1)*sensor_offset(11),...
    ones(read_entries,1)*sensor_offset(12)
    ];

% Define the indeces.
IDX_UPPER_X = 1;
IDX_UPPER_Y = 2;
IDX_LOWER_X = 3;
IDX_LOWER_Y = 4;

IDX_F_UPPER_X = 5;
IDX_F_UPPER_Y = 6;
IDX_F_LOWER_X = 7;
IDX_F_LOWER_Y = 8;

IDX_CELL = {
    [IDX_UPPER_X, IDX_UPPER_Y, IDX_F_UPPER_X, IDX_F_UPPER_Y];
    [IDX_LOWER_X, IDX_LOWER_Y, IDX_F_LOWER_X, IDX_F_LOWER_Y];
    };

nIdx = size(IDX_CELL, 1);

% % get the file name and path name form GUI
% [fn,fp] = uigetfile({'*.lvm;*.dat;*.txt','*.*'},'Selecte a file.');
% 
% if (size(fn,2) == 1 || size(fp,2) == 1)
%     disp('No file specified. Abort from script.');
%     return;
% end
% 
% % analysis the file name
% [pathstr, name, ext, versn] = fileparts([fp,fn]);

% loop for every file
row = size(ifs,1);

n_rotor_center = size(ROTOR_CENTER_COLUMS,2);
fft_results = zeros(read_entries/2 + 1,N_FFT_RESULTS*row);

% Start a stopwatch.
tic;

for I = 1:1:row
    fn = ifs{I,1};
    % analysis the file name
%     [pathstr, name, ext, versn] = fileparts([fp,fn]);
%     [pathstr, name, ext] = fileparts([fp,fn]);
    name = fn;
    
    if (1 ~= I)
        fprintf('%s\t', ifs{I,1});
    end
    
    samplesPerCycle = SAMPLE_FREQ / ifs{I,2};
    
    dbutter = designfilt(...
    'lowpassiir', 'PassbandFrequency', ifs{I,3},...
    'StopbandFrequency', ifs{I,4}, ...
    'PassbandRipple', 1,...
    'StopbandAttenuation', 60,...
    'SampleRate', fs,...
    'DesignMethod', 'butter');
    
    % get data from the file
%     disp(['get data from file...',fn]);
%     [nc,V] = getData([fp,fn],14,24,read_entries);
%     disp(['Channels = ',num2str(nc)]);
    
    [V] = get_data_ECON([fp,fn], CHANNEL_FILE_PREFIX, LINE_START, read_entries, n_rotor_center);
    
    % make new directory
    if (~isdir([out_prefix,name]))
        mkdir([out_prefix,name]);
    end
    
    % save the matrix
    save([out_prefix,name,'/V.mat'],'V','-mat');
    
    % rescale ans offet the data

    V_Scaled = ...
         V .* scale_matrix - offset_matrix;
    
    if (flag_filter)
%         V_Scaled = filter(B, A, V_Scaled);
        V_Scaled = filter(dbutter, V_Scaled);
        V_Analysis = V_Scaled(end-analysisEntries+1:end,:);
    end
    
    % Calculate fx and fy.
    fProjected = V_Analysis(:, fChannels_1) .* cos_matrix_cell{1};
    fx1 = sum(fProjected, 2);
    
    fProjected = V_Analysis(:, fChannels_1) .* sin_matrix_cell{1};
    fy1 = sum(fProjected, 2);
    
    fProjected = V_Analysis(:, fChannels_2) .* cos_matrix_cell{2};
    fx2 = sum(fProjected, 2);
    
    fProjected = V_Analysis(:, fChannels_2) .* sin_matrix_cell{2};
    fy2 = sum(fProjected, 2);
    
    
    VV = [V_Analysis(:, FFT_COLUMS), fx1, fy1, fx2, fy2];
    
    if ( 0 ~= ifs{I,2} )
        samplesForCalc = floor( floor(analysisEntries / samplesPerCycle) * samplesPerCycle );
    else
        samplesForCalc = analysisEntries;
    end
    
    if ( 1 == flag_dry_gain )
        gl = gainList(I,:);
    end
    
    % Loop for every rotor index.
    for J = 1:1:nIdx
        idxArray = IDX_CELL{J,1};
        
        if ( 1 == flag_dry_gain )
            idxOffset_gainList = (J - 1)*2;
            
            if ( 0 == flag_ucs )
                [fn, ft, ang, fnGain, ftGain] = calculate_FnFt_with_dry_gain(...
                    VV(end - samplesForCalc + 1:end,idxArray(1)) * DISPLACEMENT_SCALE,...
                    VV(end - samplesForCalc + 1:end,idxArray(2)) * DISPLACEMENT_SCALE,...
                    -VV(end - samplesForCalc + 1:end,idxArray(3)),...
                    -VV(end - samplesForCalc + 1:end,idxArray(4)),...
                    gl([idxOffset_gainList+1, idxOffset_gainList+2]));
            else
                ucsCell{J}.U95_Gnd = gl(idxOffset_gainList+5);
                ucsCell{J}.U95_Gtd = gl(idxOffset_gainList+6);
                
                [fn, ft, ang, fnGain, ftGain, ucsReturn] = ...
                    calculate_FnFt_with_dry_gain(...
                    VV(end - samplesForCalc + 1:end,idxArray(1)) * DISPLACEMENT_SCALE,...
                    VV(end - samplesForCalc + 1:end,idxArray(2)) * DISPLACEMENT_SCALE,...
                    -VV(end - samplesForCalc + 1:end,idxArray(3)),...
                    -VV(end - samplesForCalc + 1:end,idxArray(4)),...
                    gl([idxOffset_gainList+1, idxOffset_gainList+2]),...
                    ucsCell{J});
            end
        else
            if ( 0 == flag_ucs )
                [fn, ft, ang, fnGain, ftGain] = calculate_FnFt(...
                    VV(end - samplesForCalc + 1:end,idxArray(1)) * DISPLACEMENT_SCALE,...
                    VV(end - samplesForCalc + 1:end,idxArray(2)) * DISPLACEMENT_SCALE,...
                    -VV(end - samplesForCalc + 1:end,idxArray(3)),...
                    -VV(end - samplesForCalc + 1:end,idxArray(4)));
            else
                [fn, ft, ang, fnGain, ftGain, ucsReturn] = calculate_FnFt(...
                    VV(end - samplesForCalc + 1:end,idxArray(1)) * DISPLACEMENT_SCALE,...
                    VV(end - samplesForCalc + 1:end,idxArray(2)) * DISPLACEMENT_SCALE,...
                    -VV(end - samplesForCalc + 1:end,idxArray(3)),...
                    -VV(end - samplesForCalc + 1:end,idxArray(4)),...
                    ucsCell{J});
            end
        end
            
        
        if ( 1 == flag_fft )
            [ay,freq,ph] = fftAtFreq([fn,ft],SAMPLE_FREQ,0,0);
            ampFn = ay(1,1);
            ampFt = ay(1,2);
            
            [ay,freq,ph] = fftAtFreq([fnGain,ftGain],SAMPLE_FREQ,0,0);
            ampFnGain = ay(1,1);
            ampFtGain = ay(1,2);
        else
    %         ampFn = mean(fn);
    %         ampFt = mean(ft);
            ampFn = mean(fn);
            ampFt = mean(ft);
            ampFnGain = mean(fnGain);
            ampFtGain = mean(ftGain);
        end
    
        % Show timing information.
        if ( 1 == I && 1 == J)
            sw = toc;
            fprintf('The estimated time is %fs\n', sw * row);
            fprintf('%s\t', ifs{I,1});
        end
        
%     fprintf('file %d, ampFn = %e, ampFt = %e\n', I, ampFn, ampFt);
        if ( 1 == flag_gain )
            fprintf('%e\t%e\t', ampFnGain, ampFtGain);
        else
            fprintf('%e\t%e\t', ampFn, ampFt);
        end
        
        if ( 1 == flag_ucs )
            U95_Gn = sqrt( sum(ucsReturn.U95_Gni.^2) / length(ucsReturn.U95_Gni).^2 );
            U95_Gt = sqrt( sum(ucsReturn.U95_Gti.^2) / length(ucsReturn.U95_Gti).^2 );
            
            fprintf('%e\t%e\t', U95_Gn, U95_Gt);
        end
    end % J
    
    fprintf('\n');
end

sw = toc;
fprintf('%fs consumed in total.\n', sw);

