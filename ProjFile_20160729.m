% Project file.

% Flags.
flag_filter   = 1;
flag_gain     = 1;
flag_fft      = 0;
flag_dry_gain = 1;
flag_ucs      = 1; % Uncertainty Calculation Struct.

% SAMPLE_FREQ = 1280; % Hz.
SAMPLE_FREQ = 2560; % Hz.
PEAK_RATIO = 0.1;

% Load specific file according to the flags.
if ( 1 == flag_dry_gain )
%     GetGainList_20160729;
%     GetGainList_20160729_new;
    GetGainList_20160729_JFE_20171005;
end

% Input files.
input_files_20160729;

% Working directory.
fp         = 'E:/Experiments/Graduate/20160729_ECON/';
out_prefix = 'E:/Experiments/Graduate/20160729_ECON/output/';

if ( 1 == flag_ucs )
    % The following inputs should be calculated by
    % 'Uncertainties.m'.
    
    ucs1.U95_ryi = 20e-3; % mm
    ucs1.U95_rzi =  2e-3; % mm
    ucs1.U95_fyi = 705.9; % N
    ucs1.U95_fzi = 777.4; % N
    ucs1.U95_Gnd = 0;     % Raed from file.
    ucs1.U95_Gtd = 0;     % Read from file.
    
    ucs2.U95_ryi = 16e-3; % mm
    ucs2.U95_rzi = 12e-3; % mm
    ucs2.U95_fyi = 507.4; % N
    ucs2.U95_fzi = 365.2; % N
    ucs2.U95_Gnd = 0;     % Read from file.
    ucs2.U95_Gtd = 0;     % Read from file.
    
    ucsCell = {ucs1;ucs2};
end

