ifs = {
% '001',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '002',  38.0, 40.0, 80.0; % 38Hz rotate,  00Hz whirl.
% '003',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '004',  34.0, 40.0, 80.0; % 34Hz rotate,  00Hz whirl.
% '005',  32.0, 35.0, 70.0; % 32Hz rotate,  00Hz whirl.
% '006',  30.0, 35.0, 70.0; % 30Hz rotate,  00Hz whirl.
% '007',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '008',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '009',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '010',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '011',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '012',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '013',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '014',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '015',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '016',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '017',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '018',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '017_1',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '019',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '020',  38.0, 40.0, 80.0; % 38Hz rotate,  00Hz whirl.
% '021',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '022',  34.0, 40.0, 80.0; % 34Hz rotate,  00Hz whirl.
% '023',  32.0, 35.0, 70.0; % 32Hz rotate,  00Hz whirl.
% '024',  30.0, 35.0, 70.0; % 30Hz rotate,  00Hz whirl.
% '025',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '026',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '027',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '028',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '029',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '030',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '031',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '032',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '033',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '034',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '035',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '036',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '037',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
% '038',  38.0, 40.0, 80.0; % 38Hz rotate,  00Hz whirl.
% '039',  36.0, 40.0, 80.0; % 36Hz rotate,  00Hz whirl.
% '040',  34.0, 40.0, 80.0; % 34Hz rotate,  00Hz whirl.
% '041',  32.0, 35.0, 70.0; % 32Hz rotate,  00Hz whirl.
% '042',  30.0, 35.0, 70.0; % 30Hz rotate,  00Hz whirl.
% '043',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '044',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '045',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '046',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '047',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '048',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '049',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '050',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '051',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '052',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '053',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '054',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '051_1',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '055',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '056',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '057',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '058',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '059',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '060',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '061',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '062',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '063',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '064',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '065',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '066',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '067',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '068',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '069',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '070',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '071',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '072',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '073',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '074',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '075',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '076',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '077',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '078',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '079',  28.0, 35.0, 70.0; % 28Hz rotate,  00Hz whirl.
% '080',  26.0, 30.0, 60.0; % 26Hz rotate,  00Hz whirl.
% '081',  24.0, 30.0, 60.0; % 24Hz rotate,  00Hz whirl.
% '082',  22.0, 30.0, 60.0; % 22Hz rotate,  00Hz whirl.
% '083',  20.0, 30.0, 60.0; % 20Hz rotate,  00Hz whirl.
% '084',  18.0, 25.0, 50.0; % 18Hz rotate,  00Hz whirl.
% '085',  16.0, 25.0, 50.0; % 16Hz rotate,  00Hz whirl.
% '086',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '087',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '088',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '089',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '090',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
% '086_1',  14.0, 25.0, 50.0; % 14Hz rotate,  00Hz whirl.
% '087_1',  12.0, 20.0, 40.0; % 12Hz rotate,  00Hz whirl.
% '088_1',  10.0, 20.0, 40.0; % 10Hz rotate,  00Hz whirl.
% '089_1',  08.0, 20.0, 40.0; % 08Hz rotate,  00Hz whirl.
% '090_1',  06.0, 20.0, 40.0; % 06Hz rotate,  00Hz whirl.
%  '808',  40.0, 45.0, 90.0; % 40Hz rotate,  00Hz whirl.
%  '809',  20.0, 25.0, 50.0; % 20Hz rotate,  00Hz whirl.
% '091',  28.0, 35.0, 70.0; % 20Hz rotate,  28Hz whirl.
% '092',  24.0, 30.0, 60.0; % 20Hz rotate,  24Hz whirl.
% '093',  20.0, 25.0, 50.0; % 20Hz rotate,  20Hz whirl.
% '094',  16.0, 25.0, 50.0; % 20Hz rotate,  16Hz whirl.
% '095',  12.0, 25.0, 50.0; % 20Hz rotate,  12Hz whirl.
% '096',  28.0, 35.0, 70.0; % 20Hz rotate,  28Hz whirl.
% '097',  24.0, 30.0, 60.0; % 20Hz rotate,  24Hz whirl.
% '098',  20.0, 25.0, 50.0; % 20Hz rotate,  20Hz whirl.
% '099',  16.0, 25.0, 50.0; % 20Hz rotate,  16Hz whirl.
% '100',  12.0, 25.0, 50.0; % 20Hz rotate,  12Hz whirl.
% '101',  28.0, 35.0, 70.0; % 20Hz rotate,  28Hz whirl.
% '102',  24.0, 30.0, 60.0; % 20Hz rotate,  24Hz whirl.
% '103',  20.0, 25.0, 50.0; % 20Hz rotate,  20Hz whirl.
% '104',  16.0, 25.0, 50.0; % 20Hz rotate,  16Hz whirl.
% '105',  12.0, 25.0, 50.0; % 20Hz rotate,  12Hz whirl.
% '106',  28.0, 35.0, 70.0; % 26Hz rotate,  28Hz whirl.
% '107',  24.0, 30.0, 60.0; % 26Hz rotate,  24Hz whirl.
% '108',  20.0, 30.0, 60.0; % 26Hz rotate,  20Hz whirl.
% '109',  16.0, 30.0, 60.0; % 26Hz rotate,  16Hz whirl.
% '110',  12.0, 30.0, 60.0; % 26Hz rotate,  12Hz whirl.
% '111',  28.0, 35.0, 70.0; % 26Hz rotate,  28Hz whirl.
% '112',  24.0, 30.0, 60.0; % 26Hz rotate,  24Hz whirl.
% '113',  20.0, 30.0, 60.0; % 26Hz rotate,  20Hz whirl.
% '114',  16.0, 30.0, 60.0; % 26Hz rotate,  16Hz whirl.
% '115',  12.0, 30.0, 60.0; % 26Hz rotate,  12Hz whirl.
% '116',  28.0, 35.0, 70.0; % 26Hz rotate,  28Hz whirl.
% '117',  24.0, 30.0, 60.0; % 26Hz rotate,  24Hz whirl.
% '118',  20.0, 30.0, 60.0; % 26Hz rotate,  20Hz whirl.
% '119',  16.0, 30.0, 60.0; % 26Hz rotate,  16Hz whirl.
% '120',  12.0, 30.0, 60.0; % 26Hz rotate,  12Hz whirl.
% '121',  28.0, 40.0, 80.0; % 34Hz rotate,  28Hz whirl.
% '122',  24.0, 40.0, 80.0; % 34Hz rotate,  24Hz whirl.
% '123',  20.0, 40.0, 80.0; % 34Hz rotate,  20Hz whirl.
% '124',  16.0, 40.0, 80.0; % 34Hz rotate,  16Hz whirl.
% '125',  12.0, 40.0, 80.0; % 34Hz rotate,  12Hz whirl.
% '126',  28.0, 40.0, 80.0; % 34Hz rotate,  28Hz whirl.
% '127',  24.0, 40.0, 80.0; % 34Hz rotate,  24Hz whirl.
% '128',  20.0, 40.0, 80.0; % 34Hz rotate,  20Hz whirl.
% '129',  16.0, 40.0, 80.0; % 34Hz rotate,  16Hz whirl.
% '130',  12.0, 40.0, 80.0; % 34Hz rotate,  12Hz whirl.
% '131',  28.0, 40.0, 80.0; % 34Hz rotate,  28Hz whirl.
% '132',  24.0, 40.0, 80.0; % 34Hz rotate,  24Hz whirl.
% '133',  20.0, 40.0, 80.0; % 34Hz rotate,  20Hz whirl.
% '134',  16.0, 40.0, 80.0; % 34Hz rotate,  16Hz whirl.
'135',  12.0, 40.0, 80.0; % 34Hz rotate,  12Hz whirl.
% '136',  28.0, 45.0, 90.0; % 40Hz rotate,  28Hz whirl.
% '137',  24.0, 45.0, 90.0; % 40Hz rotate,  24Hz whirl.
% '138',  20.0, 45.0, 90.0; % 40Hz rotate,  20Hz whirl.
% '139',  16.0, 45.0, 90.0; % 40Hz rotate,  16Hz whirl.
% '140',  12.0, 45.0, 90.0; % 40Hz rotate,  12Hz whirl.
% '141',  28.0, 45.0, 90.0; % 40Hz rotate,  28Hz whirl.
% '142',  24.0, 45.0, 90.0; % 40Hz rotate,  24Hz whirl.
% '143',  20.0, 45.0, 90.0; % 40Hz rotate,  20Hz whirl.
% '144',  16.0, 45.0, 90.0; % 40Hz rotate,  16Hz whirl.
% '145',  12.0, 45.0, 90.0; % 40Hz rotate,  12Hz whirl.
% '146',  28.0, 45.0, 90.0; % 40Hz rotate,  28Hz whirl.
% '147',  24.0, 45.0, 90.0; % 40Hz rotate,  24Hz whirl.
% '148',  20.0, 45.0, 90.0; % 40Hz rotate,  20Hz whirl.
% '149',  16.0, 45.0, 90.0; % 40Hz rotate,  16Hz whirl.
% '150',  12.0, 45.0, 90.0; % 40Hz rotate,  12Hz whirl.
    };