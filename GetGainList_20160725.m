gainList = [
% -8.28E+05	2.63E+05	-6.86E+05	-2.13E+05 % 20Hz rotate, 28Hz whirl
% -6.71E+05	2.16E+05	-5.63E+05	-2.20E+05 % 20Hz rotate, 26Hz whirl
% -5.25E+05	1.88E+05	-4.76E+05	-2.02E+05 % 20Hz rotate, 24Hz whirl
% -3.49E+05	1.69E+05	-4.23E+05	-2.02E+05 % 20Hz rotate, 22Hz whirl
% -3.94E+05	3.24E+05	-2.66E+05	-2.36E+05 % 20Hz rotate, 20Hz whirl
% -1.82E+05	1.49E+05	-2.73E+05	-1.71E+05 % 20Hz rotate, 18Hz whirl
% -7.51E+04	1.67E+05	-2.11E+05	-1.54E+05 % 20Hz rotate, 16Hz whirl
% -2.36E+04	1.86E+05	-1.92E+05	-1.40E+05 % 20Hz rotate, 14Hz whirl
% 2.94E+04	1.49E+05	-1.27E+05	-1.63E+05 % 20Hz rotate, 12Hz whirl
% 8.05E+04	1.33E+05	-6.90E+04	-1.54E+05 % 20Hz rotate, 10Hz whirl
% 1.28E+05	1.15E+05	-8.69E+04	-1.43E+05 % 20Hz rotate,  8Hz whirl
% 1.66E+05	1.01E+05	-4.42E+04	-1.53E+05 % 20Hz rotate,  6Hz whirl
-8.24E+05	1.86E+05	-6.63E+05	-1.99E+05 % 26Hz rotate, 28Hz whirl
-6.75E+05	3.63E+05	-4.04E+05	-3.58E+05 % 26Hz rotate, 26Hz whirl
-4.65E+05	1.89E+05	-4.73E+05	-1.84E+05 % 26Hz rotate, 24Hz whirl
-3.32E+05	1.90E+05	-4.01E+05	-1.98E+05 % 26Hz rotate, 22Hz whirl
-3.03E+05	1.87E+05	-3.22E+05	-1.85E+05 % 26Hz rotate, 20Hz whirl
-1.96E+05	1.23E+05	-2.58E+05	-1.81E+05 % 26Hz rotate, 18Hz whirl
-9.04E+04	1.14E+05	-1.97E+05	-1.64E+05 % 26Hz rotate, 16Hz whirl
1.87E+03	1.15E+05	-1.89E+05	-1.52E+05 % 26Hz rotate, 14Hz whirl
4.76E+04	1.50E+05	-1.27E+05	-1.68E+05 % 26Hz rotate, 12Hz whirl
1.05E+05	1.12E+05	-7.54E+04	-1.54E+05 % 26Hz rotate, 10Hz whirl
1.34E+05	1.13E+05	-8.79E+04	-1.45E+05 % 26Hz rotate,  8Hz whirl
1.81E+05	1.20E+05	-3.89E+04	-1.62E+05 % 26Hz rotate,  6Hz whirl
-7.55E+05	1.99E+05	-6.62E+05	-1.97E+05 % 34Hz rotate, 28Hz whirl
-6.17E+05	2.06E+05	-5.57E+05	-2.14E+05 % 34Hz rotate, 26Hz whirl
-4.83E+05	1.71E+05	-4.73E+05	-1.98E+05 % 34Hz rotate, 24Hz whirl
-3.53E+05	1.57E+05	-4.02E+05	-2.05E+05 % 34Hz rotate, 22Hz whirl
-2.79E+05	1.58E+05	-3.24E+05	-1.89E+05 % 34Hz rotate, 20Hz whirl
-1.50E+05	9.87E+04	-2.61E+05	-1.86E+05 % 34Hz rotate, 18Hz whirl
-4.45E+04	1.50E+05	-2.01E+05	-1.76E+05 % 34Hz rotate, 16Hz whirl
1.53E+04	1.69E+05	-1.97E+05	-1.59E+05 % 34Hz rotate, 14Hz whirl
6.40E+04	1.58E+05	-1.30E+05	-1.78E+05 % 34Hz rotate, 12Hz whirl
1.17E+05	1.07E+05	-8.21E+04	-1.65E+05 % 34Hz rotate, 10Hz whirl
1.66E+05	1.23E+05	-9.34E+04	-1.52E+05 % 34Hz rotate,  8Hz whirl
1.93E+05	1.20E+05	-4.23E+04	-1.70E+05 % 34Hz rotate,  6Hz whirl
-8.28E+05	2.63E+05	-6.86E+05	-2.13E+05 % 20Hz rotate, 28Hz whirl
-6.71E+05	2.16E+05	-5.63E+05	-2.20E+05 % 20Hz rotate, 26Hz whirl
-5.25E+05	1.88E+05	-4.76E+05	-2.02E+05 % 20Hz rotate, 24Hz whirl
-3.49E+05	1.69E+05	-4.23E+05	-2.02E+05 % 20Hz rotate, 22Hz whirl
-3.94E+05	3.24E+05	-2.66E+05	-2.36E+05 % 20Hz rotate, 20Hz whirl
-1.82E+05	1.49E+05	-2.73E+05	-1.71E+05 % 20Hz rotate, 18Hz whirl
-7.51E+04	1.67E+05	-2.11E+05	-1.54E+05 % 20Hz rotate, 16Hz whirl
-2.36E+04	1.86E+05	-1.92E+05	-1.40E+05 % 20Hz rotate, 14Hz whirl
2.94E+04	1.49E+05	-1.27E+05	-1.63E+05 % 20Hz rotate, 12Hz whirl
8.05E+04	1.33E+05	-6.90E+04	-1.54E+05 % 20Hz rotate, 10Hz whirl
1.28E+05	1.15E+05	-8.69E+04	-1.43E+05 % 20Hz rotate,  8Hz whirl
1.66E+05	1.01E+05	-4.42E+04	-1.53E+05 % 20Hz rotate,  6Hz whirl
-8.24E+05	1.86E+05	-6.63E+05	-1.99E+05 % 26Hz rotate, 28Hz whirl
-6.75E+05	3.63E+05	-4.04E+05	-3.58E+05 % 26Hz rotate, 26Hz whirl
-4.65E+05	1.89E+05	-4.73E+05	-1.84E+05 % 26Hz rotate, 24Hz whirl
-3.32E+05	1.90E+05	-4.01E+05	-1.98E+05 % 26Hz rotate, 22Hz whirl
-3.03E+05	1.87E+05	-3.22E+05	-1.85E+05 % 26Hz rotate, 20Hz whirl
-1.96E+05	1.23E+05	-2.58E+05	-1.81E+05 % 26Hz rotate, 18Hz whirl
-9.04E+04	1.14E+05	-1.97E+05	-1.64E+05 % 26Hz rotate, 16Hz whirl
1.87E+03	1.15E+05	-1.89E+05	-1.52E+05 % 26Hz rotate, 14Hz whirl
4.76E+04	1.50E+05	-1.27E+05	-1.68E+05 % 26Hz rotate, 12Hz whirl
1.05E+05	1.12E+05	-7.54E+04	-1.54E+05 % 26Hz rotate, 10Hz whirl
1.34E+05	1.13E+05	-8.79E+04	-1.45E+05 % 26Hz rotate,  8Hz whirl
1.81E+05	1.20E+05	-3.89E+04	-1.62E+05 % 26Hz rotate,  6Hz whirl
-8.36E+05	3.24E+05	-6.27E+05	-1.88E+05 % 28Hz whirl
-5.30E+05	2.75E+05	-4.44E+05	-1.89E+05 % 24Hz whirl
-3.05E+05	2.78E+05	-2.99E+05	-1.83E+05 % 20Hz whirl
-1.04E+05	2.49E+05	-1.73E+05	-1.38E+05 % 16Hz whirl
8.77E+03	2.45E+05	-1.10E+05	-1.53E+05 % 12Hz whirl
6.93E+04	2.48E+05	-9.07E+04	-1.29E+05 %  8Hz whirl
-8.36E+05	3.24E+05	-6.27E+05	-1.88E+05 % 28Hz whirl
-5.30E+05	2.75E+05	-4.44E+05	-1.89E+05 % 24Hz whirl
-3.05E+05	2.78E+05	-2.99E+05	-1.83E+05 % 20Hz whirl
-1.04E+05	2.49E+05	-1.73E+05	-1.38E+05 % 16Hz whirl
8.77E+03	2.45E+05	-1.10E+05	-1.53E+05 % 12Hz whirl
6.93E+04	2.48E+05	-9.07E+04	-1.29E+05 %  8Hz whirl
];