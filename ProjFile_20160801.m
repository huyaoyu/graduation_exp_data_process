% Project file.

% Flags.
flag_filter        = 1;
flag_gain          = 1;
flag_fft           = 0;
flag_dry_gain      = 0;

% SAMPLE_FREQ = 1280; % Hz.
SAMPLE_FREQ = 2560; % Hz.
PEAK_RATIO = 0.1;

% Load specific file according to the flags.
if ( 1 == flag_dry_gain )
    GetGainList_20160801;
end

% Input files.
input_files_20160801;

% Working directory.
fp         = 'D:/Projects/Experiment/20160801_ECON/';
out_prefix = 'D:/Projects/Experiment/20160801_ECON/output/';
