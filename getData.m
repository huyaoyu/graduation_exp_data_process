function [nc,V] = getData(fn,loc_channels,loc_data,n)

fid = fopen(fn,'r');

for I = 1:1:loc_channels
    str = fgetl(fid);
end % I

nc = str2num(str(9:end));

for I = loc_channels+1:1:loc_data-1
    str = fgetl(fid);
end % I

V = zeros(n,nc);

for I = 1:1:n
    str   = fgetl(fid);
    value = sscanf(str,'%f',nc);
    V(I,:) = value;
end % I

fclose(fid);

end % function