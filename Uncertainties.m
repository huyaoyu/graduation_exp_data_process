% clc
clear
close all

labels1 = {'F1';'F2';'F3';'F7';};
labels2 = {'F4';'F5';'F6';'F8';};

beta1 = [ 135; -105; 15; -45 ] / 180 * pi;
beta2 = [75; -165; -45; 135] / 180 * pi;

% U_df1 = [84; 106; 72; 176] / 10;
% U_df2 = [31; 90; 64; 78] / 10;

UL1 = [0.42; 0.53; 0.36; 0.88] / 100 * 45e3;
UL2 = [0.16; 0.45; 0.32; 0.39] / 100 * 45e3;

U_df1 = UL1 * 2;
U_df2 = UL2 * 2;

U95_fyi1 = sqrt(sum(cos(beta1) .^2 .* U_df1 .^2));
U95_fzi1 = sqrt(sum(sin(beta1) .^2 .* U_df1 .^2));

U95_fyi2 = sqrt(sum(cos(beta2) .^2 .* U_df2 .^2));
U95_fzi2 = sqrt(sum(sin(beta2) .^2 .* U_df2 .^2));

N1 = length(beta1);
N2 = length(beta2);

for I = 1:1:N1
    fprintf('Uncertainty, %s, %e\n', labels1{I}, UL1(I))
end

for I = 1:1:N2
    fprintf('Uncertainty, %s, %e\n', labels2{I}, UL2(I))
end

fprintf('\n');

for I = 1:1:N1
    fprintf('U95_df, %s, %e\n', labels1{I}, U_df1(I))
end

for I = 1:1:N2
    fprintf('U95_df, %s, %e\n', labels2{I}, U_df2(I))
end

fprintf('\n');

fprintf('ucs1.U95_fyi = %f\n', U95_fyi1);
fprintf('ucs1.U95_fzi = %f\n', U95_fzi1);
fprintf('ucs2.U95_fyi = %f\n', U95_fyi2);
fprintf('ucs2.U95_fzi = %f\n', U95_fzi2);
