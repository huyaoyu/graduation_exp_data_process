function [ch] = get_data_from_file_ECON(fullFileName, lineDataStart, nLines)
% fullFileName - Full file name.
% lineDataStart - The line number of ths data start.
% nLines - The total number of data lines.
% ch - The vector of the data.

fid = fopen(fullFileName,'r');

for I = 1:1:lineDataStart-1
    str = fgetl(fid);
end % I

ch = zeros(nLines,1);

for I = 1:1:nLines
%     fprintf('%d\n', I);
%     
%     if ( I == 1025 )
%         I = I;
%     end
    
    str   = fgetl(fid);
    value = sscanf(str,'%f',1);
    ch(I,1) = value;
end % I

fclose(fid);
