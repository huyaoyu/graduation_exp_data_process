function [fn, ft, ang, fnGain, ftGain, ucsReturn] = calculate_FnFt(x, y, fx, fy, ucs)
% x,y - the coordinate vectors
% fx, fy - the forces along x and y
% fn, ft - the forces project to the normal and tangential direction
% ucs - uncertainty calculation struct.
%
% usc.U95_ryi - the expanded uncertainty for $ r_{y,i} $
% usc.U95_rzi - the expanded uncertainty for $ r_{z,i} $
% usc.U95_fyi - the expanded uncertainty for $ f_{y,i} $
% usc.U95_fzi - the expanded uncertainty for $ f_{z,i} $
%

% Check the dimensions.
[rowX, colX]   = size(x);
[rowY, colY]   = size(y);
[rowFX, colFX] = size(fx);
[rowFY, colFY] = size(fy);

if ( rowX ~= rowY || rowX ~= rowFX || rowX ~= rowFY )
    fprintf('Wrong dimension. x(%d, %d), y(%d, %d), fx(%d, %d), fy(%d, %d)\n',...
        rowX, colX, rowY, colY, rowFX, colFX, rowFY, colFY);
    
    fn = 0;
    ft = 0;
    ang = 0;
    fnGain = 0;
    ftGain = 0;
    return;
end

% Find the cetner point.

cx = mean(x);
cy = mean(y);

% Offset.

xOffset = x - cx;
yOffset = y - cy;

ang = calculate_angle(xOffset, yOffset);

fn =        fx .* cos(ang) + fy .* sin(ang);
ft = -1.0 * fx .* sin(ang) + fy .* cos(ang);

r = (xOffset.^2 + yOffset.^2).^0.5;

fnGain = fn ./ r;
ftGain = ft ./ r;

% handle ucs.

ucsReturn.U95_Gni = 0;
ucsReturn.U95_Gti = 0;

if nargin > 4
    % Calculate the uncertainty.
    temp01 = xOffset.^2 + yOffset.^2;
    temp02 = xOffset.^2 - yOffset.^2;
    temp03 = -2 .* xOffset .* yOffset;
    
    temp04 = temp02 ./ temp01;
    temp05 = temp03 ./ temp01;
    
    ucsReturn.U95_Gni = sqrt(...
          ( -temp04 .* fx + temp05 .* fy ).^2 .* ucs.U95_ryi.^2 ...
        + (  temp05 .* fx + temp04 .* fy ).^2 .* ucs.U95_rzi.^2 ...
        + ( xOffset ./ temp01 ).^2 .* ucs.U95_fyi.^2 ...
        + ( yOffset ./ temp01 ).^2 .* ucs.U95_fzi.^2 );
    
    ucsReturn.U95_Gti = sqrt(...
          ( -temp05 .* fx - temp04 .* fy ).^2 .* ucs.U95_ryi.^2 ...
        + (  temp04 .* fx - temp05 .* fy ).^2 .* ucs.U95_rzi.^2 ...
        + ( -yOffset ./ temp01 ).^2 .* ucs.U95_fyi.^2 ...
        + (  xOffset ./ temp01 ).^2 .* ucs.U95_fzi.^2 );
end
